import React from 'react';
// import { Layout, Button, Breadcrumb } from 'antd';
import Layout from '../components/Layout';
import Contests from '../containers/Contests';
import withData from '../gql-lib/apollo';
import 'antd/dist/antd.min.css';

const Index = () => (
  <Layout title="Aivivn">
    <Contests />
  </Layout>
);
export default withData(Index);
